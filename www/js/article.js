var newArticle = {
  titre: "",
  details: "",
  photo: "",
  geolocation: "",
  video: "",
};

function createArticle() {
  var titre = document.getElementById("titre");
  var details = document.getElementById("details");
  newArticle.titre = titre.value;
  newArticle.details = details.value;
  var allArticles = JSON.parse(localStorage.getItem("Articles"));
  if (allArticles) {
    allArticles.push(newArticle);
  } else {
    allArticles = [newArticle];
  }
  localStorage.setItem("Articles", JSON.stringify(allArticles));
  newArticle = {
    titre: "",
    details: "",
    photo: "",
    geolocation: "",
    video: "",
  };
  allArticles = JSON.parse(localStorage.getItem("Articles"));
  alert("Article ajouté");
}

function cameraTakePicture() {
  navigator.camera.getPicture(pictureSuccess, pictureFail, {
    quality: 80,
    destinationType: Camera.DestinationType.DATA_URL,
    targetWidth: 120,
    targetHeight: 120,
    correctOrientation: true,
  });
}

function pictureSuccess(imageData) {
  newArticle.photo = imageData;
  var image = document.getElementById("myImage");
  image.src = "data:image/jpeg;base64," + imageData;
}

function pictureFail(message) {
  alert("Failed because: " + message);
}

function geolocationSuccess(position) {
  newArticle.geolocation = {
    latitude: position.coords.latitude,
    longitude: position.coords.longitude,
  };
  var geoloc = document.getElementById("geoloc");
  geoloc.innerText =
    "Position: latitude " +
    newArticle.geolocation.latitude +
    " et longitude " +
    newArticle.geolocation.longitude;
}

function geolocationError(message) {
  alert("Failed because: " + message);
}
function geolocation() {
  navigator.geolocation.getCurrentPosition(
    geolocationSuccess,
    geolocationError,
    { timeout: 30000 }
  );
}

var videoSuccess = function(mediaFiles) {
  var path = mediaFiles[0].fullPath;
  newArticle.video = path;
  var video = document
  .getElementById("createArticle")
  video.innerHTML = "<video width='208' height='117' controls='controls'><source src='" + path + "' type='video/mp4'></video>";
};

var videoError = function(error) {
  alert("Error code: " + error.code, null, "Capture Error");
};

function takeVideo() {
  navigator.device.capture.captureVideo(videoSuccess, videoError);
}

function purgeArticles() {
  alert("Articles effacés");
  localStorage.removeItem("Articles");
}

document.getElementById("createArticle").addEventListener("click", createArticle);
document.getElementById("geolocation").addEventListener("click", geolocation);
document.getElementById("cameraTakePicture").addEventListener("click", cameraTakePicture);
document.getElementById("takeVideo").addEventListener("click", takeVideo);
document.getElementById("purgeArticles").addEventListener("click", purgeArticles);
