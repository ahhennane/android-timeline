var app = {
  // Application Constructor
  initialize: function() {
    document.addEventListener(
      "deviceready",
      this.onDeviceReady.bind(this),
      false
    );

    // document.getElementById("purge").addEventListener("click", purgeArticles);
    JSON.parse(localStorage.getItem("Articles")).forEach(article => {

      var section = document.getElementById("section");
    
      var newContent =
      `<div id="articleContent" class="ui-corner-all custom-corners">
          <div id="titleArt" class="ui-bar ui-bar-a">`+
            article.titre +
         `</div>
          <div class="ui-body ui-body-a">
              <p>`+
                article.details+
              `</p>
               <hr>`;
             if (article.geolocation != "") {
              newContent +=
              ` <p><b>Position:</b></p>
                <p class="geolocArt"> <b>Latitude</b>:` + article.geolocation.latitude  + `</p>
                <p class="geolocArt"> <b>Longitude</b>:`  + article.geolocation.longitude + `</p>`
            }
            if (article.photo != "") {
              newContent +=
                `<div id="divImgArt"> <img id ="imgArticle" src="data:image/jpeg;base64,` +
                article.photo +
                `" alt="fuck it"/></div>`;
            }
            if (article.video != "") {
              newContent += "<video width='208' height='117' controls='controls'><source src='" + article.video + "' type='video/mp4'></video>";
            }+
          `</div>`;

      section.innerHTML = newContent  +'</div><br>' + section.innerHTML;

    });

  },

  onDeviceReady: function() {
    this.receivedEvent("deviceready");
  },

  // Update DOM on a Received Event
  receivedEvent: function(id) {
    var parentElement = document.getElementById(id);
    var listeningElement = parentElement.querySelector(".listening");
    var receivedElement = parentElement.querySelector(".received");

    listeningElement.setAttribute("style", "display:none;");
    receivedElement.setAttribute("style", "display:block;");

    console.log("Received Event: " + id);
  },
};

app.initialize();
